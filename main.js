// #### Constants
const urlMain = "https://raw.githubusercontent.com/eanappi/minicms-content/master/"
const root = document.body
const converterMdToHtml = new showdown.Converter({
  metadata: true, 
  openLinksInNewWindow: true,
  emoji: true,
})
// #### Layouts
const Header = {
  view: function(vnode) {
    return m("nav", 
      m(".nav-wrapper", [
        m("a.brand-logo[href='/']", 
          {oncreate: m.route.link}, [
            m("i.material-icons", "cloud"),
            "Logo"
          ]),
        m("ul.right.hide-on-med-and-down[id='nav-mobile']", [
          m("li", 
            m("a[href='/home']",
              {oncreate: m.route.link}, "Home")),
          m("li", 
            m("a[href='/about-us']", 
              {oncreate: m.route.link}, "About Us")),
        ])
      ])
    )
  }
}
const Footer = {
  view: function() {
    return m("footer.page-footer", [
      m(".container", 
        m(".row", [
          m(".col.l12.s12", [
            m("h5.white-text", "Legales"),
            m("ul", [
              m("li", 
                m("a.grey-text.text-lighten-3[href='#!']", 
                  {oncreate: m.route.link}, "Aviso Legal")),
              m("li", 
                m("a.grey-text.text-lighten-3[href='/politica-de-cookies']", 
                  {oncreate: m.route.link}, "Politica de Cookies")),
              m("li", 
                m("a.grey-text.text-lighten-3[href='#!']", 
                  {oncreate: m.route.link}, "Política de Privacidad")),
            ])
          ])
        ])
      ),
      m(".footer-copyright", 
        m(".container", [
          "© 2018 Copyright",
          m("a.grey-text.text-lighten-4.right[href='#!']", "eanappi")
        ])
      )
    ])
  }
}
const Layout = {
  view: function(vnode) {
    return m("main", [
      m(Header),
      vnode.children,
      m(Footer)
    ])
  }
}
// #### Functions
function updateMetadataPage(metadataValue) {
  let metaDescription = document.querySelector("meta[name='description']")
  
  // Title modified by metadata from MD file
  document.title = metadataValue.title ? metadataValue.title : document.title
  // Description modified by metadata from MD file
  metaDescription.setAttribute("content", 
    metadataValue.description ? metadataValue.description : metaDescription.getAttribute("content"))
}
// #### Controllers
const Page = {
  data: "",
  metadata: null,
  amazonListId: [],
  oninit: function(vnode) {
    m.request({
      method: "GET",
      url: urlMain + vnode.attrs.key + ".md",
      deserialize: function(value) {
        return converterMdToHtml.makeHtml(value)
      },
    })
    .then(function(contentHtml) {
      Page.data = contentHtml
      Page.metadata = converterMdToHtml.getMetadata()
      updateMetadataPage(Page.metadata)
      Page.amazonListId = Page.metadata.amazon.split(",")
    })
  },
  view: function(vnode) {
    return m(Layout, [
      m(".container", m.trust(Page.data)),
      m(".container.center-align",
        m("row.amazon-grid", Page.amazonListId.map(function(idProduct) {
          return m("iframe[frameborder='0'][marginheight='0'][marginwidth='0'][scrolling='no'][src='https://rcm-eu.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=eanappi-21&language=es_ES&o=30&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=" + idProduct + "']", {style: {"width": "120px", "height": "240px"}})
        }))
      )
    ])
  }
}
// #### Routes
m.route(root, "/home", {
  "/:key": Page
})